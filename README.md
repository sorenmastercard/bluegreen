# Blue/Green deploys

The most stupidly simple blue/green deployment microservice.

You can do something like:

    curl -X POST https://whatever/bluegreen \
         -d cf_username=username@example.com \
         -d cf_password=mypassword \
         -d route_id=6f69eb89-e2c7-4346-8ee5-3209e1828a98 \
         -d current_app_guid=a68c07e8-6608-4527-8be0-a4690932715e \
         -d next_app_guid=f15835d8-714e-432c-9a4d-327bfdddb47f \
         -d delay=2

One step at a time, the number of instances serving "next app" will be
increased by 1, and the number of instances serving "current app" will
be decreased by 1, until "current app" reaches 0 and "next app"
reaches the number of instances serving "current app" when the process
started. There's a configurable delay (1 second by default) after each
"scale up" before the corresponding "scale down".
