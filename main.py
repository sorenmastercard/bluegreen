import os
import time

from cloudfoundry_client.client import CloudFoundryClient
from flask import Flask, request

app = Flask(__name__)

def get_cf_client(d):
    target_endpoint = d.get('cf_endpoint', 'https://api.run.pivotal.io')
    username = d['cf_username']
    password = d['cf_password']
    proxy = dict(http=os.environ.get('HTTP_PROXY', ''), https=os.environ.get('HTTPS_PROXY', ''))
    client = CloudFoundryClient(target_endpoint, proxy=proxy, skip_verification=False)
    client.init_with_user_credentials(username, password)
    return client

@app.route('/')
def index():
    return 'it works'

@app.route('/bluegreen', methods=['POST'])
def blue_green(info=None):
    if info is None:
        info = request.form

    delay = int(info.get('delay', 1))

    client = get_cf_client(info)
    route_id = info['route_id']
    current_app_guid = info['current_app_guid']
    next_app_guid = info['next_app_guid']

    route = client.routes.get(route_id)
    current_app = client.apps.get(current_app_guid)
    next_app = client.apps.get(next_app_guid)

    current_count = current_app['entity']['instances']
    for x in range(1, current_count+1):
        client.apps._update(next_app_guid, {'instances': x})
        time.sleep(delay)
        client.apps._update(current_app_guid, {'instances': current_count-x})
